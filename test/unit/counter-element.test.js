import '@polymer/test-fixture';
import 'axe-core/axe.min.js';
import {axeReport} from 'pwa-helpers/axe-report.js';
import '../../src/components/counter-element.js';

suite('counter-element tests', function() {
  let el;
  setup(function() {
    el = fixture('basic');
    // Make tests wait until element is rendered.
    return el.updateComplete;
  });

  test('starts empty', function() {
    assert.equal(el.clicks, '0');
    assert.equal(el.value, '0');
  });

  test('clicking on plus increments', function() {
    assert.equal(el.clicks, '0');
    assert.equal(el.value, '0');

    const buttons = el.shadowRoot.querySelectorAll('button');
    buttons[0].click();

    assert.equal(el.clicks, '1');
    assert.equal(el.value, '1');
  });

  test('clicking on minus decrements', function() {
    assert.equal(el.clicks, '0');
    assert.equal(el.value, '0');

    const buttons = el.shadowRoot.querySelectorAll('button');
    buttons[1].click();

    assert.equal(el.clicks, '1');
    assert.equal(el.value, '-1');
  });

  test('a11y', function() {
    return axeReport(el);
  });
});